package main

import "fmt"



// this is a comment
// func main() {
// 	// var name string
// 	// var quantity int 
// 	// var length, width float64

// 	// quantity = 4
// 	// length, width = 1.2 , 2.4
// 	// name = "Dorcas"

// //shorthand syntax
 
//  quantity:=4
//  name:="Dorcas"
//  length, width:=1.2, 2.4 


// fmt.Println("Hello" + " " + name)
// fmt.Println(quantity)
// fmt.Println(length + width)



// }

// func main() {
// 	fmt.Print("Enter a number: ")
// 	var input float64
// 	fmt.Scanf("%f", &input)
// 	output := input * 2
// 	fmt.Println(output)
// 	}

//control statements
// func main(){
// 	number:=1
// 	for number <=10{
// 		fmt.Println(number)
// 		number = number + 1
// 	}
// }

//if statement
func main(){
	for i:=1; i <=10; i++{
	
		fmt.Println(i)
	}

	
}

package main

import (
	"fmt"
)

type MyInt int

func (m MyInt) String() string {
	return fmt.Sprintf("This is my string function for %T. The integer referenced by this type is %d.", m, m)
}

func main() {
	var m MyInt = 1
	fmt.Println(m)
	fmt.Printf("But what if I wanted to provide custom definitions for the %% notations with my type, for example, %%d?")
}



//switch statement


// func main(){
// 	for i:=1; i <=10; i++{
// 		switch i {
// 		case 0:
// 			fmt.Println("Zero")
// 		case 1:
// 			fmt.Println("one")
// 		default:
// 			fmt.Println("Unknown Number")
// 		}
// 		}
// }

//exercise
// func main(){
// 	for i:= 1; i<=100; i ++ {
// 		if i % 3 == 0 {
// 			fmt.Println(i)
// 		}
// 	}
// }

// func main(){
// 	for i:= 1; i<=100; i ++ {
// 		if i % 3 == 0 {
// 			fmt.Println(i, "fizz")
// 		}
// 		if i % 5 == 0 {
// 			fmt.Println(i, "Buzz")
// 		}
// 		if i % 3 == 0 && i % 5 == 0{
// 			fmt.Println(i, "FizzBuzz")
// 		}else{
// 			fmt.Println(i)
// 		}
// 	}
// }

//arrays

// func main(){
// 	var x [5] int
// 	//  x[4] = 100
// 	 fmt.Println(x)
// }

