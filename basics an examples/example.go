






// Creating an interface
type myinterface interface{

	// Methods
	fun1() int
	fun2() float64
	}

























package main

import "fmt"
import "math"

//interface
type geometry interface{
	area() float32
	
}


type rect struct {
	width float32
	length float32
}

type circle struct {
	radius float32
}
	
func (c circle) area() float32{
	return math.Pi * c.radius * c.radius

}
func(r rect) area() float32{
	return r.width * r.length
}

func measure(g geometry){
	fmt.Println(g.area())
}

func main() {
	// p1:=Point{x:3}
	// fmt.Println(p1)

	mycir:=circle{radius:5}
	myrect:= rect{width:30,length:20}
	// fmt.Println(mycir.area())
	// fmt.Println(myrect.area())
	measure(mycir)
	measure(myrect)


}