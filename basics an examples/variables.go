package main

import "fmt"

// this is a comment
func main() {
	var name string
	var quantity int 
	var length, width float64

	quantity = 4
	length, width = 1.2 , 2.4
	name = "Dorcas"

//shorthand syntax
 
 quantity:=4
 name:="Dorcas"
 length, width:=1.2, 2.4 


fmt.Println("Hello" + " " + name)
fmt.Println(quantity)
fmt.Println(length + width)
}