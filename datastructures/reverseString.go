// Write a simple program that reverses a string input. 
// For example:
// Input: Sendy
// Output: ydnes

// pseudocode
// 1. create a function that takes a string as the argument
// 2. declare a variable that stores the result
// 3. loop through the string and store that particular element in the result variable
// 4. add the result to the new value of the result


package main

import "fmt"


func reverse(name string) (result string) {
    for _, value := range name {
        result = string(value) + result
        fmt.Println(result)
    }
    return result
}

func main() {
  
    // Reversing the string.
    name := "Sendy"
  
    // returns the reversed string.
    nameRev := reverse(name)
    fmt.Println(nameRev)
}