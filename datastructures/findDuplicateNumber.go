// Write a simple algorithm that returns the duplicate number in a list of integers. Assume that there is always just one repeated integer. For example:
//    Input: var nums =  []int{4, 1, 6, 8, 4}
//    Output: [4]
// pseudocode
// 1. loop through the array
// 2. reloop the array and incrememt the initial value of the element with one
// 3. declare a new variable and store the second element in it
// 4. compare the two values
// 5. print the value
package main

import "fmt"

func main()  {

	var nums = []int {5,1,6,8,5}

	for i, value:= range nums{

		for j:=i+1; j<len(nums); j++{

			value2:=nums[j]

			if value2 == value {
				
				fmt.Println(value)
			}
		}

	}
	
}