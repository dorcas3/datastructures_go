// Given an array of names, return a new array with the names that start with J.
// Input: var names = []string{"Joseph", "Clinton", "Justin", "Mark"}
// Output: [Joseph Justin]
// pseudocode
// 1. 

package main

import "fmt"
import "strings"

func sameLetter(names [] string) []string{
	var res []string
	for _, name:= range names{
		// letter:=name[0:1]
		
		if strings.ToLower(string(name[0])) == "j"{
			res = append(res,name)
		}
		
	}
	return res
	
}
func main()  {
	var names = []string{"Joseph", "Clinton", "justin", "Mark"}
	fmt.Println(sameLetter(names))
}