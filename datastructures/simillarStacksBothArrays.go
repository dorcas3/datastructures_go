// Given two arrays containing tech stacks, write a program that displays the stack(s) contained in both arrays. For example:

// Input 1: var frontend = []string{"CSS", "HTML", "JavaScript", "Vue.js"} 
// Input 2: var backend = []string{"Java", "Go", "JavaScript", "PHP"}


package main

import "fmt"



func techStacks(frontend,backend [] string) string {
	var result string
	for _, value:= range frontend{
		for _, value2:= range backend{
			if value ==  value2{
				result = value
			}
		}
	}
	return result
	
}
func main()  {
	var frontend = []string{"CSS", "HTML", "JavaScript", "Vue.js"} 
	var backend = []string{"Java", "Go", "JavaScript", "PHP"}

	fmt.Println(techStacks(frontend,backend))
}