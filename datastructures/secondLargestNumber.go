// Write a simple program that returns the second largest number from an array. For example:
//    Input: var nums =  []int{43, 11, 6, 81, 34}
//    Output: [43]

package main

import "fmt"
import "sort"
// pseudocode
// 1. sort the array
// 2. reverse the sorted array
// 3. print the number at second last index


func main()  {
	var nums =  []int{43, 11, 6, 81, 34}
	sort.Ints(nums)
	// reverseArray:= reverse(sortedArray)
	fmt.Println((nums[len(nums)-2]))
	
	
}