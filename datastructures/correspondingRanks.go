// Write a simple program that takes in an array of integers and returns an array containing their corresponding ranks. For example:
// Input: {34, 5, 8, 90}
// Output: {3, 1, 2, 4}

package main

import "fmt"


func rank(arr []int) [4]int{
	var result [4]int
	   for i:=0; i<len(arr); i++{
	   count:= 0
	   for j:=0; j<len(arr); j++{
		  if arr[j] < arr[i]{
			 count ++
		  }
	   }
	   result[i] = count +1
	}
	return result
 }
 func main() {
	   ints := []int{34, 5, 8, 90}
	fmt.Println(rank(ints))
 }